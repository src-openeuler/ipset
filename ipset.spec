Name:	          ipset	
Version:  	  7.19
Release:	  1
Summary:	  Manage Linux IP sets
License:	  GPLv2
URL:		  http://ipset.netfilter.org/
Source0:	  http://ipset.netfilter.org/%{name}-%{version}.tar.bz2
Source1:          ipset.service
Source2:          ipset.start-stop
Source3:          ipset-config


BuildRequires:	  libmnl-devel automake autoconf libtool libtool-ltdl-devel systemd make
Requires:         ipset-libs = %{version}-%{release} iptables-services
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd


Provides:         ipset-service
Obsoletes:        ipset-service

%description
IP sets are a framework inside the Linux kernel, which can be administered 
by the ipset utility. Depending on the type, an IP set may store IP 
addresses, networks, (TCP/UDP) port numbers, MAC addresses, interface names 
or combinations of them in a way, which ensures lightning speed when 
matching an entry against a set.

If you want to
  * store multiple IP addresses or port numbers and match against the 
    collection by iptables at one swoop;
  * dynamically update iptables rules against IP addresses or ports without 
    performance penalty;
  * express complex IP address and ports based rulesets with one single 
    iptables rule and benefit from the speed of IP sets
then ipset may be the proper tool for you.

%package           libs
Summary:           library files for ipset

%description       libs
library files for ipset

%package           devel
Summary:           Header files for ipset
Requires:          ipset-libs = %{version}-%{release} kernel-headers

%description       devel
Header files for ipset

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
./autogen.sh
%configure --with-kmod=no --disable-static
rm -fr kernel
%disable_rpath
%make_build

%install
%make_install
%delete_la

install -d -m 755 %{buildroot}/%{_unitdir}
install -c -m 644 %{SOURCE1} %{buildroot}/%{_unitdir}

install -d -m 755 %{buildroot}%{_libexecdir}/%{name}
install -c -m 755 %{SOURCE2} %{buildroot}%{_libexecdir}/%{name}

install -d -m 755 %{buildroot}%{_sysconfdir}/sysconfig
install -c -m 600 %{SOURCE3} %{buildroot}%{_sysconfdir}/sysconfig/%{name}-config

install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}
	
%ldconfig_scriptlets libs

%post
%systemd_post %{name}.service
if [[ -f /etc/ipset/ipset ]] && [[ ! -f /etc/sysconfig/ipset ]]; then
    mv /etc/ipset/ipset /etc/sysconfig/ipset
    ln -s /etc/sysconfig/ipset /etc/ipset/ipset
    echo "Warning: ipset save location has moved to /etc/sysconfig"
fi
[[ -f /etc/sysconfig/iptables-config ]] && . /etc/sysconfig/iptables-config
[[ -f /etc/sysconfig/ip6tables-config ]] && . /etc/sysconfig/ip6tables-config
if [[ ${IPTABLES_SAVE_ON_STOP} == yes ]] || \
   [[ ${IP6TABLES_SAVE_ON_STOP} == yes ]]; then
    echo "Warning: ipset no longer saves automatically when iptables does"
    echo "         must enable explicitly in /etc/sysconfig/ipset-config"
fi

%preun
if [[ $1 -eq 0 && -n $(lsmod | grep "^xt_set ") ]]; then
    rmmod xt_set 2>/dev/null
    [[ $? -ne 0 ]] && echo Current iptables configuration requires ipsets && exit 1
fi
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%defattr(-,root,root)
%license COPYING
%doc ChangeLog
%{_sbindir}/ipset
%{_sbindir}/ipset-translate
%{_unitdir}/ipset.service
%{_libexecdir}/ipset/ipset.start-stop
%{_sysconfdir}/%{name}
%config(noreplace) %attr(0600,root,root) %{_sysconfdir}/sysconfig/ipset-config
%ghost %config(noreplace) %attr(0600,root,root) %{_sysconfdir}/sysconfig/ipset

%files             libs
%defattr(-,root,root)
%license COPYING
%{_libdir}/libipset.so.13*

%files             devel
%defattr(-,root,root)
%{_includedir}/libipset
%{_libdir}/libipset.so
%{_libdir}/pkgconfig/*.pc

%files              help
%defattr(-,root,root)
%{_mandir}/man8/%{name}*.8.*
%{_mandir}/man3/libipset.3.*

%changelog
* Mon Dec 25 2023 xinghe <xinghe2@h-partners.com> - 7.19-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update ipset to 7.19

* Tue Feb 28 2023 gaihuiying <eaglegai@163.com> - 7.17-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:delete *.a file

* Wed Feb 01 2023 xinghe <xinghe2@h-partners.com> - 7.17-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update ipset to 7.17

* Thu Oct 20 2022 xinghe <xinghe2@h-partners.com> - 7.15-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix IPv6 sets nftables translation

* Mon Mar 21 2022 yanglu <yanglu72@h-partners.com> - 7.15-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC: update ipset to 7.15

* Fri Jan 22 2021 xihaochen<xihaochen@huawei.com> - 7.10-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC: update ipset to 7.10

* Mon Jun 29 2020 gaihuiying <gaihuiying1@huawei.com> - 7.6-1
- Type:requirement
- Id:NA
- SUG:NA
- DESC:update ipset version from 7.3 to 7.6

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.3-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:modify the spec

* Wed Sep 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.3-1
- Package init
